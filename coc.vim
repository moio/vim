"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" GLOBAL EXTENSIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:coc_global_extensions = [ 'coc-git', 'coc-tabnine', 'coc-tsserver', 'coc-css', 'coc-yaml', 'coc-json', 'coc-prettier', 'coc-eslint', 'coc-cssmodules', 'coc-lightbulb' ]

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" HELPER FUNCTIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! ShowDocIfNoDiagnostic(timer_id)
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#float#has_float() == 0)
    silent call CocActionAsync('doHover')
  endif
endfunction

function! s:show_hover_doc()
  call timer_start(10, 'ShowDocIfNoDiagnostic')
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" COMPLETITION & HINTS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" start coc completition
inoremap <silent><expr> <c-x><c-x> coc#refresh()

" gh - get hint on whatever's under the cursor
nnoremap <silent> K :call <SID>show_hover_doc()<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NAVIGATION
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" Use `sj` and `sk` to navigate diagnostics
	nmap <silent> <LocalLeader>k <Plug>(coc-diagnostic-prev)
	nmap <silent> <LocalLeader>j <Plug>(coc-diagnostic-next)

	" goto definition, type, implementation and reference
	nmap <silent> <LocalLeader>D <Plug>(coc-definition)
	nmap <silent> <LocalLeader>T <Plug>(coc-type-definition)
	nmap <silent> <LocalLeader>I <Plug>(coc-implementation)
	nmap <silent> <LocalLeader>R <Plug>(coc-references)
	nmap <LocalLeader>a <Plug>(coc-codeaction-line)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" LISTS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	nnoremap <silent> <LocalLeader>l :<C-u>CocList<cr>
	nnoremap <silent> <LocalLeader>c :<C-u>CocList commands<cr>
	vnoremap <silent> <LocalLeader>c :<C-u>CocList commands<cr>
	nnoremap <silent> <LocalLeader>p :<C-u>CocListResume<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ACTIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	nmap <LocalLeader>r <Plug>(coc-rename)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" TEXT OBJECTS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	"" Map function and class text objects
	"" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
	xmap if <Plug>(coc-funcobj-i)
	omap if <Plug>(coc-funcobj-i)
	xmap af <Plug>(coc-funcobj-a)
	omap af <Plug>(coc-funcobj-a)
	xmap ic <Plug>(coc-classobj-i)
	omap ic <Plug>(coc-classobj-i)
	xmap ac <Plug>(coc-classobj-a)
	omap ac <Plug>(coc-classobj-a)

