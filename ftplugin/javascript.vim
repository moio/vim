" add typescript suffixes for :h gf
setlocal suffixesadd+=.json
setlocal wildignore+=node_modules/*

"Imprt sort
nmap <leader>oi mj:/import/,?import? sort /\/[A-z]/<cr>`j

" define convenience sniplets
iabbrev log console.log();<left><left>
iabbrev warn console.warn();<left><left>
iabbrev debug console.debug();<left><left>

" in after/ftplugin/javascript.vim
setlocal errorformat=%f:\ line\ %l\\,\ col\ %c\\,\ %m,%-G%.%#

augroup js, ts
	autocmd!
	autocmd BufWritePost silent <buffer> make! <afile> | silent redraw
augroup END

""""""""""""""""
" macro mappings
""""""""""""""""

" Class state init to useState hook
command! ToHook exe 'norm _ywiconst [<esc>f:i, <esc>pF,wvUiset<esc>f:i] <esc>lr=wiuseState(<esc>$i)<esc>f,r;j'

command! ToIfElse exe 'norm _iif (<esc>f?s){<esc>f:s} else {<esc>A}<esc>?(.*){w'

command! ToMemoized exe 'norm _3cf const <esc>w"tywf(i = React.memo(<esc>l%a =><esc>f{%a);<esc>oexport default <esc>"tpA;<esc>bgd'
