"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible

" Map leader & localleader
let mapleader = " "
let maplocalleader = "s"

set updatetime=100

" This option helps to avoid all the |hit-enter| prompts caused by file
set shortmess+=cat

" Enable filetype plugins
filetype plugin on
filetype indent on

set foldmethod=indent
set foldlevel=999

set noruler

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Sourced
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("win16") || has("win32")
	let $VIM_CONFIG_HOME=$HOME . "/vimfiles"
	if has("nvim")
		set runtimepath^=$VIM_CONFIG_HOME runtimepath+=$VIM_CONFIG_HOME/after
		let &packpath=&runtimepath
	endif
else
	let $VIM_CONFIG_HOME=$HOME . "/.vim"
endif

" Link to helper functions file.
source $VIM_CONFIG_HOME/functions.vim

" Link to plugins file.
source $VIM_CONFIG_HOME/plugins.vim

" Link to keybindings file.
source $VIM_CONFIG_HOME/keybindings.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => File shortcuts commands
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Opens the user vimrc file
command! EditVimrc exe 'e $VIM_CONFIG_HOME/vimrc'

" Opens the defined keybindings
command! EditBindings exe 'e $VIM_CONFIG_HOME/keybindings.vim'

" Opens the plugin config
command! EditPlugins exe 'e $VIM_CONFIG_HOME/plugins.vim'

" Opens the plugin config
command! EditFunctions exe 'e $VIM_CONFIG_HOME/functions.vim'

" Opens the plugin for the open buffer filetype
command! EditFTPlugin exe 'e $VIM_CONFIG_HOME/ftplugin/' . &filetype . '.vim'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => UI
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set 4 lines to the cursor
set scrolloff=4

" Show typed keys
set showcmd

" Turn on the Wild menu
set wildmenu

set wildignorecase

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
	set wildignore+=.git\*,.hg\*,.svn\*
else
	set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store,**/node_modules/**,*/build
endif

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" always show sign column
set signcolumn=yes

" show status line when there is more then one pane
set laststatus=1

" always show tab line
set showtabline=2

" show expanded path in tabline
set tabline=%!MyTabLine()

" Set utf8 as standard encoding
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" enable syntax highlighting
syntax on

set background=dark

" use gui colors
se termguicolors

" Update colors when colorscheme is changed
augroup update_theme
    autocmd!
    autocmd ColorScheme * call Update_theme()
augroup END

color bronzage

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups, History and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Sets how many lines of history VIM has to remember
set history=500

" Turn backup off, since most stuff is in SVN, git etc. anyway...
let $SWAPDIR=$VIM_CONFIG_HOME . '/swapdir'
if empty(glob($SWAPDIR))
	call mkdir($SWAPDIR)
endif
set nobackup
set noswapfile
set directory=$SWAPDIR

"  Turn persistent undo on
let $UNDODIR=$VIM_CONFIG_HOME . '/undodir'

if empty(glob($UNDODIR))
	call mkdir($UNDODIR)
endif
set undodir=$UNDODIR
set undofile

" session automatons for when entering or leaving Vim
autocmd VimEnter * nested :call LoadSession()
autocmd VimLeave * :call MakeSession()

" Make it so that if files are changed externally (ex: changing git branches)
" update the vim buffers automatically
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI *
			\ if mode() != 'c' && getcmdwintype() == '' | checktime | endif
autocmd FileChangedShellPost *
			\ echohl WarningMsg
			\ | echo "File changed on disk. Buffer reloaded."
			\ | echohl None

" do not store global values in a session
set ssop-=options

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Only one space gets added when you join two lines.
set nojoinspaces

" Wrapped lines stay at the same indentation level.
set breakindent

" adds and delete tabs based on shiftwidth and tabstop
set smarttab

" number of space for autoindent
set shiftwidth=2

" number of space for a tab
set tabstop=2

set autoindent
set smartindent
set wrap "Wrap lines
set linebreak
if has('nvim')
	set listchars=tab:\│\ ,multispace:\ \ \╎\ \ \┊,trail:¶ " indent lines and trailing whitespaces
endif

" show '$: ' at start of wrapped lines
set showbreak=>\

" delete trailing whitespace on save
autocmd BufWritePre * :call CleanExtraSpaces()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" abbreviate %% to current file directory
cabbr %% <C-R>=expand('%:p:h')<CR>

set path+=src/**,app/**,main/**,static/,config/

" Specify the behavior when switching between buffers
try
	set switchbuf=useopen
catch
endtry

" A buffer becomes hidden when it is abandoned
set hidden

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()

" " open quickfix if there is something
" augroup minivimrc
" 	autocmd!
" 	autocmd QuickFixCmdPost [^l]* cwindow
" 	autocmd QuickFixCmdPost    l* lwindow
" 	autocmd VimEnter            * cwindow
" augroup END

cnoremap <expr> <CR> CCR()

" Return to last edit position when opening files
"autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
