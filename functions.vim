"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! <SID>BufcloseCloseIt()
	let l:currentBufNum = bufnr("%")
	let l:alternateBufNum = bufnr("#")

	if buflisted(l:alternateBufNum)
		buffer #
	else
		bnext
	endif

	if bufnr("%") == l:currentBufNum
		new
	endif

	if buflisted(l:currentBufNum)
		execute("bdelete! ".l:currentBufNum)
	endif
endfunction

" Delete trailing white space
function! CleanExtraSpaces()
	let save_cursor = getpos(".")
	let old_query = getreg('/')
	silent! %s/\s\+$//e
	call setpos('.', save_cursor)
	call setreg('/', old_query)
endfun

" Save session when closing vim and restore it when opening
function! MakeSession()
  let b:sessiondir = $VIM_CONFIG_HOME . "/./sessions" . getcwd()
  if (filewritable(b:sessiondir) != 2)
    exe 'silent !mkdir -p ' b:sessiondir
    redraw!
  endif
  let b:filename = b:sessiondir . '/session.vim'
  exe "mksession! " . b:filename
endfunction

" Load last session if vim is started withouth args
function! LoadSession()
  let b:sessiondir = $VIM_CONFIG_HOME . "/./sessions" . getcwd()
  let b:sessionfile = b:sessiondir . "/session.vim"
  if (filereadable(b:sessionfile) && argc() == 0)
    exe 'source ' b:sessionfile
  else
    echo "No session loaded."
  endif
endfunction

" Check if plugin is loaded
function! PlugLoaded(name)
	return !empty(glob("$VIM_CONFIG_HOME/plugged/" . a:name))
endfunction

" make transparent
function! Update_theme()
	" transparent background - use term colors
	hi SignColumn guibg=none ctermbg=none
	hi Normal guibg= none ctermbg=none
	hi NonText guibg=none ctermbg=None guifg=3 ctermfg=3
	hi VertSplit guifg=0 guibg=0 ctermfg=0 ctermbg=0
	" search and cursor over parenthesis more visible
	hi search ctermbg=Black ctermfg=White cterm=bold,underline
	hi MatchParen cterm=bold ctermbg=none ctermfg=229
	hi MatchParen gui=bold guibg=none guifg=none
endfunction

" make list-like commands more intuitive
function! CCR()
	let cmdline = getcmdline()
	if cmdline =~ '\v\C^(ls|files|buffers)'
		" like :ls but prompts for a buffer command
		return "\<CR>:b"
	elseif cmdline =~ '\v\C/(#|nu|num|numb|numbe|number)$'
		" like :g//# but prompts for a command
		return "\<CR>:"
	elseif cmdline =~ '\v\C^(dli|il)'
		" like :dlist or :ilist but prompts for a count for :djump or :ijump
		return "\<CR>:" . cmdline[0] . "j  " . split(cmdline, " ")[1] . "\<S-Left>\<Left>"
	elseif cmdline =~ '\v\C^(cli|lli)'
		" like :clist or :llist but prompts for an error/location number
		return "\<CR>:sil " . repeat(cmdline[0], 2) . "\<Space>"
	elseif cmdline =~ '\C^old'
		" like :oldfiles but prompts for an old file to edit
		return "\<CR>:sil se more|e #<"
	elseif cmdline =~ '\C^changes'
		" like :changes but prompts for a change to jump to
		return "\<CR>:sil se more|norm! g;\<S-Left>"
	elseif cmdline =~ '\C^ju'
		" like :jumps but prompts for a position to jump to
		return "\<CR>:sil se more|norm! \<C-o>\<S-Left>"
	elseif cmdline =~ '\C^marks'
		" like :marks but prompts for a mark to jump to
		return "\<CR>:norm! `"
	elseif cmdline =~ '\C^undol'
		" like :undolist but prompts for a change to undo
		return "\<CR>:u "
	else
		return "\<CR>"
	endif
endfunction

function! BreakHere()
	s/^\(\s*\)\(.\{-}\)\(\s*\)\(\%#\)\(\s*\)\(.*\)/\1\2\r\1\4\6
	call histdel("/", -1)
endfunction


" search upward from to current file for a parent folder that contains a file or folder with the specified name
function! FindProjectRoot(lookFor)
	let pathMaker='%:p'
	while(len(expand(pathMaker))>len(expand(pathMaker.':h')))
		let pathMaker=pathMaker.':h'
		let fileToCheck=expand(pathMaker).'/'.a:lookFor
		if filereadable(fileToCheck)||isdirectory(fileToCheck)
			return expand(pathMaker)
		endif
	endwhile
	return 0
endfunction


" set tabline numer and label
function MyTabLine()
	let s = ''
	for i in range(tabpagenr('$'))
		" select the highlighting
		if i + 1 == tabpagenr()
			let s .= '%#TabLineSel#'
		else
			let s .= '%#TabLine#'
		endif

		" set the tab page number (for mouse clicks)
		let s .= '%' . (i + 1) . 'T'

		" the label is made by MyTabLabel()
		let s .= ' %{MyTabLabel(' . (i + 1) . ')} '
	endfor

	" after the last tab fill with TabLineFill and reset tab page nr
	let s .= '%#TabLineFill#%T'

	" right-align the label to close the current tab page
	if tabpagenr('$') > 1
		let s .= '%=%#TabLine#%999Xclose'
	endif

	return s
endfunction

" shoe each page label from cwd
function MyTabLabel(n)
	let buflist = tabpagebuflist(a:n)
	let winnr = tabpagewinnr(a:n)
	let name = bufname(buflist[winnr - 1])
	if (name == '')
		if (&filetype ==# 'netrw')
			let name = 'netrw'
		else
			let name = "no name"
		endif
	endif
	if (&modified != "modified")
		let name = "+ " . name
	endif
	return name
endfunction
