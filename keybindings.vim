"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" Fast saving
	nmap <leader>w :w!<cr>

	" to select registers mostly
	noremap <Leader> "

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Keyboard layout
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" (IT)
	map ò :

	map ì <C-^>

	nmap è [
	nmap + ]
	omap è [
	omap + ]
	xmap è [
	xmap + ]

	nmap à {
	nmap ù }
	omap à {
	omap ù }
	xmap à {
	xmap ù }

	nnoremap ç 
	xnoremap ç 
	onoremap ç 
	inoremap ç 

""""""""""""""""""""""""""""""
" => Navigation
""""""""""""""""""""""""""""""

	" Fuzzy finder for files
	nmap <leader>e :e **/*

	" Pressing <Leader> 2 times begin searching
	nmap <Leader><Leader> /
	omap <Leader><Leader> /
	vmap <Leader><Leader> /

	" Don't move the cursor when using * or #
	nnoremap * *N
	nnoremap # #N
	nnoremap g* g*N
	nnoremap g# g#N

	nnoremap j gj
	nnoremap k gk

	nnoremap ' `

""""""""""""""""""""""""""""""
" => Buffers
""""""""""""""""""""""""""""""
"
	" Switch CWD to the directory of the open buffer
	nnoremap <leader>cd :lcd %:p:h<cr>:pwd<cr>

	" ie = inner entire buffer
	onoremap ie :exec "normal! ggVG"<cr>

	" iv = current viewable text in the buffer
	onoremap iv :exec "normal! HVL"<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" format buffer
	nnoremap <Leader>= gg=G''

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => yanking, pasting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" Y yank from cursor till end
	nmap Y y$

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Terminal Mode
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" Esc and jj makes terminal navigavble
	tnoremap <Esc><Esc> <C-w>N

	" ì and ^ switch to last buffer
	tnoremap ì <C-w>N<C-^>
	tnoremap ^ <C-w>N<C-^>

	tnoremap <silent> <c-w>h :TmuxNavigateLeft<cr>
	tnoremap <silent> <c-w>j :TmuxNavigateDown<cr>
	tnoremap <silent> <c-w>k :TmuxNavigateUp<cr>
	tnoremap <silent> <c-w>l :TmuxNavigateRight<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Insert Mode
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" recover from <c-u> undo
	inoremap <c-u> <c-G>u<c-u>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

	" split line here like a reverse J
	nnoremap S :<C-u>call BreakHere()<CR>

	" Open file note
	map <leader>q :e ~/notes<cr>
