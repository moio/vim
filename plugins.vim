"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Manager Setup
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Install vim-plug if missing
if empty(glob('$VIM_CONFIG_HOME/autoload/plug.vim'))
	if has("unix")
		silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
					\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
	elseif has("win32")
		echo "run iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim | ni $HOME/vimfiles/autoload/plug.vim -Force"
	endif
endif

" Install missing plugins on Vim startup
autocmd VimEnter *
			\  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
			\|   PlugInstall --sync | q
			\| endif

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin("$VIM_CONFIG_HOME/plugged")

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins List
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Make sure you use single quotes
" Multiple Plug commands can be written in a single line using | separators

""""""""""""""
" GENERAL
""""""""""""""

Plug 'mbbill/undotree', { 'on': 'UndotreeShow' } " tree for undo navigation
Plug 'neoclide/coc.nvim', {'branch': 'release'} " VSCode ecosystem and Language Servers
Plug 'takac/vim-hardtime'
Plug 'tommcdo/vim-exchange' " exchange text objects
Plug 'tpope/vim-abolish' " add case coercion operations and case independant replace
Plug 'tpope/vim-commentary' " add mappings for comment manipulation
Plug 'tpope/vim-repeat' " repeat operation works for plugin defined operations
Plug 'tpope/vim-surround' " manipulation of text objects surrounding
Plug 'tpope/vim-unimpaired' " add mapping for switching vim options
Plug 'tpope/vim-vinegar'

""""""""""""""
" AI integration
""""""""""""""

Plug 'aduros/ai.vim'

""""""""""""""
" NAVIGATION
""""""""""""""

Plug 'chrisbra/matchit' " extended matching
Plug 'wellle/targets.vim' " new text object and operations

""""""""""""""
" SYNTAX
""""""""""""""
Plug 'sheerun/vim-polyglot' " language syntax support

""""""""""""""
"	UI
""""""""""""""

Plug 'machakann/vim-highlightedyank' " highlight yanked text
Plug 'romainl/vim-cool' " turn  hlsearch off when moving cursor
"Plug 'ap/vim-css-color' " hex color highlighter

if has('nvim')
	Plug 'psliwka/vim-smoothie' " scroll animation
endif

" display ex commands result while typing
if has('nvim')
	set inccommand=nosplit
else
	Plug 'markonm/traces.vim'
endif

""""""""""""""
"	THEMES
""""""""""""""

Plug 'Michal-Miko/vim-mono-red'
Plug 'romainl/apprentice'
Plug 'habamax/vim-bronzage'
Plug 'git@gitlab.com:yorickpeterse/happy_hacking.vim.git'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => coc.vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:coc_config_home = $VIM_CONFIG_HOME
let g:coc_data_home = $VIM_CONFIG_HOME . '/coc'
let $COC_CONFIG_FILE=$VIM_CONFIG_HOME . '/coc.vim'

hi CocHighlightText ctermbg=NONE ctermfg=NONE cterm=standout

command! EditCocConfig exe 'e $COC_CONFIG_FILE'
source $COC_CONFIG_FILE

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim grep
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

command! -nargs=+ -complete=file_in_path -bar Grep  silent! grep! <args> | redraw!

set grepprg=grep\ -inrHE\ --exclude-dir=node_modules


""""""""""""""
" ai.vim
""""""""""""""

let g:ai_timeout=60

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins Configuration Ending
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:netrw_preview = 1

" Initialize plugin system
call plug#end()

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
